poetry shell
poetry run pip uninstall lxml
poetry run pip uninstall loguru
poetry run pip uninstall pyinstaller
poetry run pip install --upgrade lxml
poetry run pip install --upgrade loguru
poetry run pip install --upgrade pyinstaller
cd docx_modify || exit
poetry run pyinstaller --noconfirm --distpath "../bin" docx_modify.exe.spec