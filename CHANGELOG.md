# Updates #

## v1.3.1 ##

* Added logo selection due to the decimal number if detected, otherwise Protei ST one;
* Refactored output and log information;

## v1.3.1 ##

* Fixed GUI elements;
* Removed PySide6 option;

## v1.3.0 ##

* Added placeholders in footers and headers to replace due to the user input;
* Added DOCPROPERTY \_Page_Sheet_ for different document sides;
* Added an option to append Change List to the document;
* Fixed GUI element layout;

## v1.2.3 ##

* Added support of the docm format;
* Fixed title page formatting for arch formatting mode;
* Added searching and specifying custom document properties;

## v1.2.2 ##

* Added processing files formatted for the Ministry of Defense;
* Added processing files formatted as archived;
* Refactored the GUI elements;

## v1.2.1 ##

* Added processing files with landscape pages;
* Added help information while running;

## v1.2.0 ##

* Added capability of processing several files;
* Added the inspection of file types;
* Restructured module architecture and file separating;
* Added content to the README.md file;

## v1.1.0 ##

* Formatted headers and footers;
* Added GUI elements to get user files and desired formatting mode;
* Generated executable files;

## v1.0.0 ##

* The main features are realized;
