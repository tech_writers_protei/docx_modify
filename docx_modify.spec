# -*- mode: python ; coding: utf-8 -*-
from PyInstaller.utils.hooks import collect_dynamic_libs

binaries = []
binaries += collect_dynamic_libs('.')


a = Analysis(
    ['main.py'],
    pathex=['.', '../venv/lib/python3.11/site-packages'],
    binaries=binaries,
    datas=[('../sources', 'sources/'), ('../LICENSE', '.'), ('../MANIFEST.in', '.'), ('../pyproject.toml', '.'), ('../README.adoc', '.'), ('../requirements.txt', '.')],
    hiddenimports=['loguru', 'lxml', 'PySide6'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.datas,
    [],
    name='docx_modify',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
