git clone git@gitlab.com:tech_writers_protei/docx_modify.git

# shellcheck disable=SC1073
# shellcheck disable=SC1072
# shellcheck disable=SC1009
if [[ "$OSTYPE" in msys*) ]]; then
	cd .\docx_modify\docx_modify
	py -m pip install poetry
  poetry shell
  pyinstaller --noconfirm --onefile --console --name "docx_modify.exe" --distpath "../bin" \
  --add-data "../sources;sources/" \
  --add-data "../docs;docs/" \
  --add-data "../LICENSE;." \
  --add-data "../MANIFEST.in;." \
  --add-data "../pyproject.toml;." \
  --add-data "../README.adoc;." \
  --add-data "../requirements.txt;." \
  --collect-binaries "." \
  --paths "." --paths "../venv/Lib/site-packages" \
  --hidden-import "loguru" --hidden-import "lxml" main.py
  ..\bin\docx_modify.exe
elif [[ "$OSTYPE" in linux*) | "$OSTYPE" in darwin*) ]]; then
	cd docx_modify/docx_modify
	python3 -m pip install poetry
	poetry shell
  pip install --upgrade pyinstaller
  pyinstaller --noconfirm --onefile --console --name "docx_modify" --distpath "../bin" \
  --add-data "../sources:sources/" \
  --add-data "../docs:docs/" \
  --add-data "../LICENSE:." \
  --add-data "../MANIFEST.in:." \
  --add-data "../pyproject.toml:." \
  --add-data "../README.adoc:." \
  --add-data "../requirements.txt:." \
  --collect-binaries "." \
  --paths "." --paths "../venv/lib/python3.11/site-packages" \
  --hidden-import "loguru" --hidden-import "lxml" main.py
  ../bin/docx_modify
fi
