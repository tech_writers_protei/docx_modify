# -*- coding: utf-8 -*-
from importlib import import_module
from pathlib import Path
from subprocess import CalledProcessError, run
from sys import executable, modules, version_info
from warnings import filterwarnings

from docx_modify.exceptions import InvalidPythonVersion

_parent_path: Path = Path(__file__).parent.parent
_temp_path: Path = Path.home().joinpath("Desktop")
_log_folder: Path = Path.home().joinpath("Desktop").joinpath("_docx_logs")

filterwarnings("ignore")

if version_info < (3, 8):
    print("Версия Python должна быть не менее 3.8")
    input("Нажмите <Enter>, чтобы закрыть окно ...")
    raise InvalidPythonVersion

_packages: tuple[str, ...] = ("loguru", "lxml")
for _package in _packages:
    if _package not in modules:
        try:
            import_module(_package)
        except ModuleNotFoundError and ImportError:
            print(f"Попытка загрузить модуль {_package}")
            try:
                run(
                    [executable, "-m", "pip", "install", "--upgrade", _package], timeout=45.0).check_returncode()
            except CalledProcessError as e:
                print(f"{e.__class__.__name__}\nКод ответа {e.returncode}\nОшибка {e.output}")
                print(f"Не удалось импортировать пакет `{_package}`")
                raise
            except OSError as e:
                print(f"{e.__class__.__name__}\nФайл {e.filename}\nОшибка {e.strerror}")
                print(f"Не удалось импортировать пакет `{_package}`")
                raise

if not _log_folder.exists():
    _log_folder.mkdir(parents=True, exist_ok=True)


if __name__ == '__main__':
    try:
        from file_processing import run_script

        run_script()
    except ModuleNotFoundError:
        print(
            "If your system is macOS, install Tk through the terminal command \n"
            "brew install python-tk")
        input()
