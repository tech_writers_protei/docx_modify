# -*- coding: utf-8 -*-
from pathlib import Path
from tkinter import Tk, IntVar, TclError
from tkinter.constants import FLAT
from tkinter.font import Font
from tkinter.ttk import Style

from loguru import logger

from docx_modify.enum_element import UserInputValues, DocumentMode, DocumentSide
from docx_modify.exceptions import InvalidOptionError
from docx_modify.gui.constants import get_files, root_title, _dict_document_side_cb, root_geometry, \
    place_document_side, place_checkbuttons, place_buttons, place_document_mode
from docx_modify.gui.custom_element import CustomFrame, FrameDocumentMode, FrameDocumentSide, FrameOkCancel, \
    FrameCheckbuttons


class Application:
    def __init__(self, root: Tk):
        self._root: Tk = root
        self._root.wm_title(root_title)
        self._user_input_values: UserInputValues | None = UserInputValues()
        self._paths: list[str] = []
        self._document_mode: IntVar = IntVar()
        self._document_side: IntVar = IntVar()
        self._def_ministry: IntVar = IntVar()
        self._change_list: IntVar = IntVar()
        self._approvement_list: IntVar = IntVar()
        self._frames: dict[str, CustomFrame] = dict()

        self._root.geometry(root_geometry)
        self._root.bind("<Escape>", self.quit)
        self._root.wm_resizable(False, False)
        self._root.protocol("WM_DELETE_WINDOW", self.x_button)

        radiobutton_font: Font = Font(
            family="Times",
            name="rbf.TRadiobutton",
            size=16,
            weight="bold",
            slant="roman",
            underline=False,
            overstrike=False)

        button_font: Font = Font(
            family="Times",
            name="bf.TButton",
            size=14,
            weight="normal",
            slant="roman",
            underline=False,
            overstrike=False)

        checkbutton_font: Font = Font(
            family="Times",
            name="cbf.TCheckbutton",
            size=16,
            weight="bold",
            slant="roman",
            underline=False,
            overstrike=False
        )

        label_font: Font = Font(
            family="Times",
            name="lf.TLabel",
            size=18,
            weight="bold",
            slant="roman",
            underline=False,
            overstrike=False)

        s: Style = Style(self._root)

        s.configure("l.TLabel", font=label_font)

        s.configure(
            "b.TButton",
            font=button_font,
            padx=5,
            pady=5,
            borderwidth=1,
            relief=FLAT)

        s.configure(
            "rb.TRadiobutton",
            font=radiobutton_font,
            padding=[5, 5, 5, 5],
            padx=5,
            pady=5,
            relief=FLAT)

        s.configure(
            "cb.TCheckbutton",
            font=checkbutton_font,
            padding=[5, 5, 5, 5],
            padx=5,
            pady=5,
            relief=FLAT)

        s.configure(
            "l.TLabel",
            font=label_font)

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._frames.get(item)
        else:
            raise KeyError

    def __bool__(self):
        return bool(self._root.winfo_exists())

    @property
    def names(self):
        return self._frames.keys()

    def x_button(self):
        self._user_input_values = None
        logger.info("Работа программы прервана пользователем ...")
        self._root.destroy()

    def show_frame(self, name: str, **kwargs) -> None:
        if name in self.names:
            frame: CustomFrame = self._frames.get(name)
            frame.show()
            frame.full_place(**kwargs)
        else:
            logger.info(f"Frame {name} is not found")

    def hide_frame(self, name: str) -> None:
        if name in self.names:
            frame: CustomFrame = self._frames.get(name)
            frame.hide()
            frame.place_forget()
        else:
            logger.info(f"Frame {name} is not found")

    def add_frame(self, frame: CustomFrame, **kwargs):
        self._frames[frame.winfo_name()] = frame
        frame.full_place(**kwargs)
        self._root.update()

    def ok_command(self):
        logger.info(f"document mode button pressed = {self._document_mode.get()}")
        logger.info(f"document side button pressed = {self._document_side.get()}")
        logger.info(f"def ministry button pressed = {self._def_ministry.get()}")
        logger.info(f"change list button pressed = {self._change_list.get()}")
        logger.info(f"approvement list button pressed = {self._approvement_list.get()}")

        # noinspection PySimplifyBooleanCheck
        if self._document_mode.get() == 0:
            self._user_input_values._document_mode = DocumentMode.TYPO
            self._user_input_values._document_side = DocumentSide.MIRROR
            self._user_input_values._def_ministry = False
            self._user_input_values._change_list = False
            self._user_input_values._approvement_list = False

        elif self._document_mode.get() == 1:
            self._user_input_values._document_mode = DocumentMode.ARCH
            self._user_input_values._document_side = _dict_document_side_cb.get(self._document_side.get(), None)
            self._user_input_values._def_ministry = bool(self._def_ministry.get())
            self._user_input_values._change_list = bool(self._change_list.get())
            self._user_input_values._approvement_list = bool(self._approvement_list.get())

        else:
            logger.error(f"Некорректный выбор варианта")
            raise InvalidOptionError

        self._root.destroy()

        return self._user_input_values

    def cancel_command(self):
        logger.info("Cancel button is pressed")
        self._user_input_values = None
        self._root.destroy()

    def add_frame_document_mode(self):
        def hide_command():
            self.hide_frame("document_side")
            self.hide_frame("checkbuttons")

        def show_command():
            self.hide_frame("ok_cancel")
            self.show_frame("document_side", **place_document_side)
            self.show_frame("checkbuttons", **place_checkbuttons)
            self.show_frame("ok_cancel", **place_buttons)

        frame_document_mode: FrameDocumentMode = FrameDocumentMode(self._root)
        frame_document_mode.add_document_mode(
            self._document_mode,
            arch_command=show_command,
            typo_command=hide_command)
        self.add_frame(frame_document_mode)

    def add_frame_document_archive(self):
        frame_document_archive: FrameDocumentSide = FrameDocumentSide(self._root)
        frame_document_archive.add_document_side(self._document_side)
        self.add_frame(frame_document_archive)

    def add_frame_checkbuttons(self):
        frame_checkbuttons: FrameCheckbuttons = FrameCheckbuttons(self._root)
        frame_checkbuttons.add_approvement_list(self._approvement_list)
        frame_checkbuttons.add_change_list(self._change_list)
        frame_checkbuttons.add_def_ministry(self._def_ministry)
        self.add_frame(frame_checkbuttons)

    def add_frame_ok_cancel(self):
        frame_ok_cancel: FrameOkCancel = FrameOkCancel(self._root)
        frame_ok_cancel.add_buttons(ok_command=self.ok_command, cancel_command=self.cancel_command)
        self.add_frame(frame_ok_cancel)

    def run(self):
        try:
            self._root.wm_withdraw()

            files: tuple[str, ...] | None = get_files()

            if files is None:
                self._root.destroy()
            else:
                self._user_input_values._path_files = [Path(file) for file in files]

            self._root.wm_deiconify()

            self.add_frame_document_mode()
            self.add_frame_document_archive()
            self.add_frame_checkbuttons()
            self.add_frame_ok_cancel()

            self._frames.get("document_mode").place(**place_document_mode)
            self._frames.get("document_side").place(**place_document_side)
            self._frames.get("checkbuttons").place(**place_checkbuttons)
            self._frames.get("ok_cancel").place(**place_buttons)

            for name, frame in self._frames.items():
                if bool(frame):
                    self.show_frame(name)
                else:
                    self.hide_frame(name)

        except TclError as e:
            logger.info("Работа программы прервана пользователем ...")
            logger.info(f"{e.__class__.__name__}, {str(e)}, {e.args}")
            self._user_input_values = None

        except InvalidOptionError:
            logger.info("Некорректная опция")
            self._root.destroy()
            logger.info(self._user_input_values)

    @property
    def user_input_values(self):
        return self._user_input_values

    def quit(self, _) -> None:
        self._root.quit()


def get_user_input():
    root: Tk = Tk()
    application: Application = Application(root)
    application.run()
    root.mainloop()
    user_input_values: UserInputValues = application.user_input_values
    return user_input_values


if __name__ == '__main__':
    print(repr(get_user_input()))
