# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable, Iterator

from loguru import logger
from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import ElementBase, _ElementTree

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.core_elements.core_zip_file import CoreZipFile
from docx_modify.enum_element import _STYLES_BASIC, _STYLES_CHANGE_LIST
from docx_modify.main import _parent_path
from docx_modify.xml_elements.xml_file import XmlFile


class XmlStyles(XmlFile):
    def __init__(self, core_zip_file: CoreZipFile, basic_file: Path, styles: Iterable[str] = None):
        if styles is None:
            styles: list[str] = []

        name: str = "word/styles.xml"
        super().__init__(name, core_zip_file)
        self._basic_file: Path = basic_file
        self._styles: tuple[str, ...] = *styles,

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def iter_styles(self) -> Iterator[str]:
        return iter(self._styles)

    def add_styles(self):
        self.read()
        _styles_id: tuple[str, ...] = tuple(child.get(fqdn("w:styleId")) for child in iter(self))
        logger.info(f"{self.__class__.__name__}._styles_id = \n{_styles_id}")

        for style in self.iter_styles():
            if style in _styles_id:
                _style_xml: ElementBase | None = self.find_child("w:styleId", style)
                self.delete_child(_style_xml)
                logger.info(f"Style {style} is deleted")

        _et: _ElementTree = etree.parse(self._basic_file)
        _root: ElementBase = _et.getroot()

        for file_style in tuple(_root):
            self.add_child(file_style)
            logger.info(f"Style {file_style.get(fqdn('w:styleId'))} is added")

        self.write()
        logger.info("XML styles are updated")
        return


class XmlBasicStyles(XmlStyles):
    def __init__(self, core_zip_file: CoreZipFile):
        basic_file: Path = _parent_path.joinpath(f"sources/styles/styles.xml")
        styles: tuple[str, ...] = _STYLES_BASIC
        super().__init__(core_zip_file, basic_file, styles)


class XmlChangeListStyles(XmlStyles):
    def __init__(self, core_zip_file: CoreZipFile):
        basic_file: Path = _parent_path.joinpath(f"sources/change_list_styles/styles.xml")
        styles: tuple[str, ...] = _STYLES_CHANGE_LIST
        super().__init__(core_zip_file, basic_file, styles)
