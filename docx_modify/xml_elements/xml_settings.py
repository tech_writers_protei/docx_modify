# -*- coding: utf-8 -*-
from loguru import logger
from lxml.etree import ElementBase

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.core_elements.core_zip_file import CoreZipFile
from docx_modify.enum_element import DocumentSide, DocumentMode
from docx_modify.xml_elements.xml_element_factory import new_xml
from docx_modify.xml_elements.xml_file import XmlFile


class XmlSettings(XmlFile):
    def __init__(self, core_zip_file: CoreZipFile, side: DocumentSide, document_mode: DocumentMode):
        name: str = "word/settings.xml"
        super().__init__(name, core_zip_file)
        self._document_side: DocumentSide = side
        self._document_mode: DocumentMode = document_mode

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    def _set_update_fields(self):
        self.delete_child_if_exists("w:updateFields")
        update_fields: ElementBase = new_xml("w:updateFields")
        self.add_child(update_fields)
        logger.info("Auto update before opened is implemented")

    def _set_mirror_margins(self):
        mirror_margins: ElementBase = self.get_or_add_child("w:mirrorMargins")

        if self._document_side == DocumentSide.MIRROR:
            mirror_margins.set(fqdn("w:val"), "1")
            logger.info("Mirror margins are implemented")
        elif self._document_side == DocumentSide.SINGLE:
            mirror_margins.set(fqdn("w:val"), "0")
            logger.info("Mirror margins are deleted")
        return

    def _set_borders_do_not_surround(self):
        self.delete_child_if_exists("w:bordersDoNotSurroundHeader")
        self.delete_child_if_exists("w:bordersDoNotSurroundFooter")
        borders_do_not_surround_header: ElementBase = new_xml("w:bordersDoNotSurroundHeader")
        borders_do_not_surround_footer: ElementBase = new_xml("w:bordersDoNotSurroundFooter")

        if self._document_mode == DocumentMode.ARCH:
            borders_do_not_surround_header.set(fqdn("w:val"), "0")
            borders_do_not_surround_footer.set(fqdn("w:val"), "0")
            logger.info("Headers and Footers are surrounded")
        elif self._document_mode == DocumentMode.TYPO:
            borders_do_not_surround_header.set(fqdn("w:val"), "1")
            borders_do_not_surround_footer.set(fqdn("w:val"), "1")
            logger.info("Headers and Footers are not surrounded")

    def _set_even_and_odd_headers(self):
        even_and_odd_headers: ElementBase = self.get_or_add_child("w:evenAndOddHeaders")

        if self._document_side == DocumentSide.MIRROR:
            even_and_odd_headers.set(fqdn("w:val"), "1")
            logger.info("Even and odd headers are implemented")
        elif self._document_side == DocumentSide.SINGLE:
            even_and_odd_headers.set(fqdn("w:val"), "0")
            logger.info("Even and odd headers are disabled")
        return

    def _set_parameter(self, child: str, tag: str | None = None, value: str | None = None):
        self.delete_child_if_exists(child)

        if tag is not None:
            attributes: dict[str, str] = {tag: f"{value}"}
            logger.info(f"Child {child} attribute <{tag}> = {value}")
        else:
            attributes: dict[str, str] = dict()
            logger.info(f"Child {child} is added")

        _param: ElementBase = new_xml(child, attributes=attributes)
        self.add_child(_param)
        return

    def set_settings(self):
        self.read()
        self._set_mirror_margins()
        self._set_even_and_odd_headers()
        self._set_borders_do_not_surround()

        self._set_parameter("w:alignBordersAndEdges")
        logger.info("Alignment of borders and edges is implemented")

        self.write()
        return
